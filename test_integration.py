import json
from io import StringIO

from togglr import Togglr, Toggle, OnOffCondition
from test_util import IdInCondition


def test_all_toggles_disabled():
    togglr = Togglr()
    assert not togglr.is_active("a-feature", {})

def test_with_on_off_condition():
    togglr = Togglr()

    cond = OnOffCondition()
    togglr.register(Toggle("a-feature", conditions=[cond]))

    assert not togglr.is_active("a-feature", {})

    cond.value = True

    assert togglr.is_active("a-feature", {})

class TestUser:
    def __init__(self, id):
        self.id = id

def test_with_list_of_user_ids():
    togglr = Togglr()
    cond = IdInCondition()
    togglr.register(Toggle("a-feature", conditions=[cond]))
    user = TestUser(17)

    assert not togglr.is_active("a-feature", dict(user=user))

    cond.value = [17, 28]

    assert togglr.is_active("a-feature", dict(user=user))
