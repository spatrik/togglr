import json
import inspect
import importlib

class Togglr:
    def __init__(self):
        self._registry = {}

    def is_active(self, name, context):
        if name not in self._registry:
            return False

        return self._registry[name].is_active(context)

    def register(self, toggle):
        self._registry[toggle.name] = toggle

    @property
    def toggles(self):
        return self._registry.values()


class Toggle:
    def __init__(self, name, conditions=[]):
        self.name = name
        self._conditions = []

        for condition in conditions:
            self.add_condition(condition)

    def add_condition(self, condition):
        if len(self._conditions) == 1:
            raise ValueError("Togglr currently only supports one condition")
        self._conditions.append(condition)

    def is_active(self, context):
        cond = self._conditions[0]
        return cond.match(context)


class OnOffCondition:
    def __init__(self):
        self.value = False

    def match(self, context):
        return self.value


class JSONFileStore:
    def __init__(self, fp):
        self._file = fp

    def save(self, togglr):
        def serialize_cond(cond):
            return {
                "class": "{mod}.{class_name}".format(
                    mod=cond.__class__.__module__,
                    class_name=cond.__class__.__name__,
                ),
                "value": cond.value,
            }
        out = {"toggles": []}
        for toggle in togglr._registry.values():
            conditions = [serialize_cond(cond) for cond in toggle._conditions]
            out["toggles"].append(
                {
                    "name": toggle.name,
                    "conditions": conditions,
                }
            )

        json.dump(out, self._file)

    def load(self):
        togglr = Togglr()
        loaded_json = json.load(self._file)

        for toggle_attrs in loaded_json["toggles"]:
            toggle = Toggle(toggle_attrs["name"])

            for cond_attrs in toggle_attrs["conditions"]:
                def class_from_name(name):
                    parts = name.split(".")
                    mod_name = ".".join(parts[0:-1])
                    class_name = parts[-1]

                    mod = importlib.import_module(mod_name)
                    return getattr(mod, class_name)

                cond_class = class_from_name(cond_attrs["class"])
                cond = cond_class()
                cond.value = cond_attrs["value"]
                toggle.add_condition(cond)
                togglr.register(toggle)


        return togglr
