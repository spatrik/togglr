## Togglr – Feature Flags for Python

Togglr is a lib for doing feature flags in python.

## Current status

There is only a README, no code yet :)

## Requirements

It is based on the following requirements:

- A feature flag must be able to be controlled in the following ways:
    - on or off globally
    - based on an attribute on the current user
    - based on an attribute on something other than the current user
    - a percentage of user
    - a combination of the above (eg a percentage of all users with the is_admin flag set)
- Changes in the conditions of the flags must be changeable without doing a
  deploy (but an app restart is ok, if it simplifies things)

## Synopsis

Assume we have a system where we have forums and users.  This is an example
where we have a feature named `my-feature` and only want to show it when the
current user is staff and the current forum is a certain forum.

    # Bara vissa forum om man är staff
    toggle = Toggle("my-feature")
    toggle.add_condition(ForumSlugs())
    toggle.conditions[0].value = ["awesome-forum"]
    toggle.add_condition(IsStaff())

    togglr.active("my-feature", forum=a_forum, user=a_user)            # => False
    togglr.active("my-feature", forum=a_forum, user=staff_user)        # => False
    togglr.active("my-feature", forum=awesome_forum, user=a_user)      # => False
    togglr.active("my-feature", forum=awesome_forum, user=staff_user)  # => True
    togglr.active("my-feature", user=staff_user)  # => TODO: How to handle this case?

The first part will be something that is normally done in a web UI or maybe a
config file. The second part is how it is used when you want to check if a
feature is active for a collection of inputs

## Conditions

A Condition is an object that responds to `match`. In the simplest case,
`match` takes no params. The return value of `match` will be interpreted as a
bool and used to decide if the feature is active or not.

A `match` without arguments is only useful to implement a on/off condition. To
make more interesting conditions, `match` can also take any number of
positional arguments. When Togglr calls the `match` method on a Condition, it
will extract the kwargs in the `active` call with the same name as the
arguments and use this as values for the arguments.

TODO: Decide what happens if `active` is called without all the required arguments

## Persistance

TODO

## Percentage

TODO

Important: Needs to keep the same ids in the active group when increasing the percentage

## Future

- togglr-django: Integration with django