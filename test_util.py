class IdInCondition:
    def __init__(self):
        self.value = []

    def match(self, context):
        user = context["user"]
        return user.id in self.value
