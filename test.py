# encoding: utf-8
import inspect


class Togglr(object):
    def __init__(self):
        self._registry = {}

    def active(self, name, **kwargs):
        match = True
        for cond in self._registry[name].conditions:
            args = self._match_args_for(cond)

            cond_kwargs = {k: v for k, v in kwargs.items() if k in args}
            match &= cond.match(**cond_kwargs)

        return match

    def _match_args_for(self, cond):
        cond_class = type(cond)
        argspec = inspect.getargspec(cond_class.match)
        match_args = argspec.args[1:]
        return match_args

    def register(self, toggle):
        self._registry[toggle.name] = toggle

class Toggle(object):
    def __init__(self, name):
        self.name = name
        self._conditions = []

    def add_condition(self, cond):
        self._conditions.append(cond)

    @property
    def conditions(self):
        return self._conditions


class ForumSlugs(object):
    def __init__(self):
        self.value = []

    def match(self, forum):
        return forum.slug in self.value


class IsStaff(object):
    def match(self, user):
        return user.is_staff


class User(object):
    def __init__(self, is_staff):
        self.is_staff = is_staff

class Forum(object):
    def __init__(self, slug):
        self.slug = slug


if __name__ == "__main__":
    a_user = User(False)
    staff_user = User(True)
    a_forum = Forum("a-forum")
    kundo_forum = Forum("kundo")

    togglr = Togglr()

    # Bara vissa forum om man är staff
    toggle = Toggle("my-feature")
    toggle.add_condition(ForumSlugs())
    toggle.conditions[0].value = ["kundo"]
    toggle.add_condition(IsStaff())
    togglr.register(toggle)

    print(togglr.active("my-feature", forum=a_forum, user=a_user))  # => False
    print(togglr.active("my-feature", forum=a_forum, user=staff_user))  # => False
    print(togglr.active("my-feature", forum=kundo_forum, user=a_user)) # => False
    print(togglr.active("my-feature", forum=kundo_forum, user=staff_user))  # => True
    #print togglr.active("my-feature", user=staff_user)  # => Hur hantera?
    
    print("adding a-forum")
    toggle.conditions[0].value = ["kundo", "a-forum"]

    print(togglr.active("my-feature", forum=a_forum, user=a_user))  # => False
    print(togglr.active("my-feature", forum=a_forum, user=staff_user))  # => False
    print(togglr.active("my-feature", forum=kundo_forum, user=a_user))  # => False
    print(togglr.active("my-feature", forum=kundo_forum, user=staff_user))  # => True
    

class Takes(object):
    @classmethod
    def args(cls):
        argspec = inspect.getargspec(cls.match)
        match_args = argspec.args[1:]
        return match_args
    
class TakesUser(Takes):
    def match(self, user):
        print("Got a user: {}".format(user))

class TakesForum(Takes):
    def match(self, forum, kaka):
        print("Got a forum: {}".format(forum))

class TakesForumAndKaka(Takes):
    def match(self, forum, kaka):
        print("Got a forum: {} and a kaka {}".format(forum, kaka))


#things = [TakesUser(), TakesForum(), TakesForumAndKaka()]

def active(name, **kwargs):
    for t in things:
        thing_class = type(t)

        args = thing_class.args()

        t_kwargs = {k: v for k, v in kwargs.items() if k in args}
        t.match(**t_kwargs)

# forum = "forum"
# user = "user"
# kaka = "kaka"
# active("foo", forum=forum, user=user, kaka=kaka)
