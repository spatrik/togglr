import json
from io import StringIO

from togglr import Togglr, Toggle, OnOffCondition, JSONFileStore
from test_util import IdInCondition

def test_with_json_file_persistance():
    fp = StringIO()
    store = JSONFileStore(fp=fp)
    togglr = Togglr()

    toggle = Toggle("my-feature")
    cond = OnOffCondition()
    cond.value = True
    toggle.add_condition(cond)
    togglr.register(toggle)

    store.save(togglr)

    fp.seek(0)
    togglr2 = store.load()
    assert togglr2.is_active("my-feature", {})

def test_json_save():
    fp = StringIO()
    store = JSONFileStore(fp=fp)
    togglr = Togglr()
    toggle = Toggle("my-feature")
    cond = OnOffCondition()
    cond.value = True
    toggle.add_condition(cond)
    togglr.register(toggle)
    store.save(togglr)

    expected = {
        "toggles": [
            {
                "name": "my-feature",
                "conditions": [
                    {
                        "class": "togglr.OnOffCondition",
                        "value": True,
                        }
                ]
            }
        ]
    }

    assert json.dumps(expected) == fp.getvalue()

def test_json_ronudtrip_with_custom_condition():
    fp = StringIO()
    store = JSONFileStore(fp=fp)
    togglr = Togglr()
    toggle = Toggle("my-feature")
    cond = IdInCondition()
    cond.value = True
    toggle.add_condition(cond)
    togglr.register(toggle)
    store.save(togglr)

    result = json.loads(fp.getvalue())

    fp.seek(0)
    togglr2 = store.load()

    assert isinstance(
        togglr2._registry["my-feature"]._conditions[0],
        IdInCondition
    )


def test_load_json():
    input_json = json.dumps({
        "toggles": [
            {
                "name": "my-feature",
                "conditions": [
                    {
                        "class": "togglr.OnOffCondition",
                        "value": True,
                        }
                ]
            }
        ]
    })
    input_io = StringIO(input_json)

    store = JSONFileStore(fp=input_io)
    togglr = store.load()

    assert len(togglr._registry) == 1
    assert "my-feature" in togglr._registry
    assert len(togglr._registry["my-feature"]._conditions) == 1
    assert isinstance(togglr._registry["my-feature"]._conditions[0], OnOffCondition)
